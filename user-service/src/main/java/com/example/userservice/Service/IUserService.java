package com.example.userservice.Service;

import com.example.userservice.Entity.User;
import org.springframework.stereotype.Service;

@Service
public interface IUserService {
    User save(User user);

    User getUserByUserId(String userId);
}
