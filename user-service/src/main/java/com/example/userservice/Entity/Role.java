package com.example.userservice.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Role {
    @Id
    @Column(length = 4)
    private int roleId;

    @Column(length = 10)
    private String roleName;

    @JsonManagedReference
    @OneToMany(mappedBy = "role", targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<User> users;
}
