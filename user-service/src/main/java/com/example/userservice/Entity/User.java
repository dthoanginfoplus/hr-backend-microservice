package com.example.userservice.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {
    @Id
    @Size(min=6, max=20,message = "userId invalid length")
    @Column(length = 20)
    private String userId;

    @Size(max = 255,message = "password invalid length")
    @Column(length = 255)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Size(min = 1, max = 255, message = "Full name invalid length")
    private String fullName;

    @Size(max = 10, message = "Phone number invalid")
    @Column(length = 10)
    private String phoneNumber;

    @Size(min = 6, max = 255, message = "Email invalid length")
    @Email(message = "Email invalid")
    @Column(length = 255)
    private String email;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "roleId")
    private Role role;
}
